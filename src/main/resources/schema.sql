CREATE TABLE IF not exists `account`  (
    `user_id` varchar(100) NOT NULL,
    `id1` varchar(100) NOT NULL,
    `id2` varchar(100) NOT NULL,
    PRIMARY KEY (`user_id`),
    UNIQUE INDEX `u_id1_id2`(`id1`, `id2`) USING BTREE COMMENT 'unique index of id1 and id2'
);