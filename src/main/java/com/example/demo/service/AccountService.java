package com.example.demo.service;

import com.example.demo.entity.Account;
import com.example.demo.repo.AccountRepo;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public Account getOrInit(Account queryParam) {
        String cacheKey = "Account:" + queryParam.getId1() + ":" + queryParam.getId2();
        String userID = redisTemplate.opsForValue().get(cacheKey);

        if (Optional.ofNullable(userID).filter(Strings::isNotBlank).isPresent()) {
            return queryParam.withUserID(userID);
        }

        Account account = accountRepo.getAccountById1AndAndId2(queryParam.getId1(), queryParam.getId2());
        if (Optional.ofNullable(account).isPresent()) {
            redisTemplate.opsForValue().set(cacheKey, account.getUserID());
            return account;
        }

        return accountRepo.save(queryParam.withUserID(null));
    }
}
