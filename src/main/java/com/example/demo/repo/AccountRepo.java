package com.example.demo.repo;

import com.example.demo.entity.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepo extends CrudRepository<Account, String> {

    Account getAccountById1AndAndId2(String id1, String id2);
}
