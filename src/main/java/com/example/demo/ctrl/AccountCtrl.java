package com.example.demo.ctrl;

import com.example.demo.entity.Account;
import com.example.demo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(path = "/account")
public class AccountCtrl {

    @Autowired
    AccountService accountService;

    @PostMapping(path = "/login")
    public Map<String, String> login(@RequestBody Account queryParam) {
        Account account = accountService.getOrInit(queryParam);

        Map<String, String> result = new HashMap<>();
        result.put("userID", account.getUserID());
        return result;
    }
}
